# Cloud AI Challedge

Express anagram checker api

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Note: This project is not production ready.

### Prerequisites

What things you need to install the software and how to install them

```
npm
nodeJS
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Install with `npm install`
npm run dev
```

And setup secret

```
change the sampleEnv at the project root to .env
```

## Built With

* [NodeJS](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [ExpressJS](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing


## Authors

* **Ighor Jesse** - *Initial work* - [Elayira](https://gitlab.com/ayira)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
