import unscramBleAnagram from './anagram'
import anagramChecker from '../unscrambler'
import notFound from './not-found'

const anagramTest = unscramBleAnagram(anagramChecker)
const anagramController = Object.freeze({
    anagramTest, unscramBleAnagram, notFound
})

export default anagramController
export {anagramTest, unscramBleAnagram, notFound}
