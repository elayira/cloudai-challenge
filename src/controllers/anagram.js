export default function unscramBleAnagram(anagramChecker) {
    return async function(httpRequest) {
      try {
        const { source = {}, ...words } = httpRequest.body
        source.ip = httpRequest.ip
        source.browser = httpRequest.headers['User-Agent']
        if (httpRequest.headers['Referer']) {
          source.referrer = httpRequest.headers['Referer']
        }
        const result = await anagramChecker(words)
        return {
          headers: {
            'Content-Type': 'application/json',
          },
          statusCode: 201,
          body: {
            ...result,
          }
        }
      } catch (e) {
        // TODO: Error logging
        console.log(e)

        return {
          headers: {
            'Content-Type': 'application/json'
          },
          statusCode: 400,
          body: {
            error: e.message
          }
        }
      }
    }
  }
