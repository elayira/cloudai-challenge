import {default as anagramTest} from './anagram'
import {buildAnagram} from '../helpers/anagram'

const anagramChecker = buildAnagram(anagramTest)
export default anagramChecker
