export default function anagramChecker(word, altword) {
    word = word.split('')
    altword = altword.split('')
    return word.size === altword.size
        ? word.every( value => altword.includes(value) )
        : false
}
