import {ErrorHandler} from '../controllers/error'
function buildAnagram(makeSource) {
    return function({word, altword}) {
        if (!(word && altword)) {
            throw new ErrorHandler(404, 'word and altword must have values')
        }
        return Object.freeze({
            word,
            altword,
            createdOn: Date.now(),
            isAnagram: makeSource(word, altword)
        })
    }
}

export {buildAnagram}
