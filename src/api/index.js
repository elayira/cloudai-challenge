import {notFound as unFound} from '../controllers'
import {handleError} from '../controllers/error'
import anagramRouter from './anagram'
import makeCallback from '../express-callback'
const notFound = makeCallback(unFound)

export {
    notFound,
    handleError,
    anagramRouter
}
