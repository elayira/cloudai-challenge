import express from 'express'
import {anagramTest} from '../controllers'
import makeCallback from '../express-callback'

var anagramRouter = express.Router()

anagramRouter.post('/anagram', makeCallback(anagramTest))

export default anagramRouter
