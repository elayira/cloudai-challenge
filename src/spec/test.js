import { expect } from 'chai'

import anagramChecker from '../unscrambler'


describe('anagramChecker function', () => {
    it('should return true', () => {
        expect(anagramChecker('arc', 'car')).to.eql(true)
    })
    it('should return false', () => {
        expect(anagramChecker('act', 'car')).to.eql(false)
    })
    it('should return false', () => {
        expect(anagramChecker('', '')).to.eql(true)
    })
    it('should return false', () => {
        expect(anagramChecker('car ', 'arc ')).to.eql(true)
    })
})
