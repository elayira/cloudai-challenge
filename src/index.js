import express from 'express'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import {
  notFound,
  handleError,
  anagramRouter
} from './api'

dotenv.config()

const apiRoot = process.env.UNSCRAMBLER_ENV_API_ROOT
const app = express()
app.use(bodyParser.json())
// TODO: figure out DNT compliance.
app.use((_, res, next) => {
  res.set({ Tk: '!' })
  next()
})
app.use(`${apiRoot}/`, anagramRouter)
app.use(notFound)
app.use((err, req, res, next) => {
  handleError(err, res);
})

if (process.env.UNSCRAMBLER_ENV === 'dev') {
  // listen for requests
  app.listen(3000, () => {
    console.log('Server is listening on port 3000')
  })
}

export default app
